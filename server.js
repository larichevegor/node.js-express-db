var express = require('express'),
  bodyParser = require('body-parser'),
  path = require('path'),
  cons = require('consolidate'),
  dust = require('dustjs-helpers'),
  pg = require('pg'),
  app = express();
Sequelize = require('sequelize');

// DB connect string
var config = {
  user: 'Larichev', //
  database: 'MyDB',
  password: '1234', //
  host: 'localhost', // Server hosting the postgres database
  port: 5432, //env var: PGPORT
};

const sequelize = new Sequelize('myDB', 'Larichev', '1234', {
  host: 'localhost',
  dialect: 'postgres',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
});

app.engine('dust', cons.dust);

// Set defaults Ext .dust
app.set('view engine', 'dust');
app.set('views', __dirname + '/views');

// Set public Folder
app.use(express.static(path.join(__dirname, 'public')));

//Body Parser Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Server
var pgClient = new pg.Client(config);
let client;

pgClient.connect(function (err, pgConnectionClient) {
  if (err) {
    return console.log(err);
  };
  client = pgConnectionClient;
  app.listen(3000, function () {
    console.log("Server Started on PORT 3000");
  });
});


/*  const query = client.query(
  'CREATE TABLE TasksList(id SERIAL PRIMARY KEY, text VARCHAR(40) not null, complete BOOLEAN)');
query.on('end', () => { client.end(); })*/


app.get('/', (req, res, next) => {
  res.sendFile(path.join(
    __dirname, '..', 'building-api-with-node', 'public', 'views', 'index.html'));

});

// Добавление заданий
app.post('/tasks', (req, res, next) => {
  const results = [];
  const data = { text: req.body.text, complete: false };
  //client = pgClient;
  const query = client.query('INSERT INTO taskslist(text, complete) values($1, $2)',
    [data.text, data.complete]);
  query.on('end', () => {
    return res.json(results);
  });
});


// Вывод заданий
app.get('/tasks', (req, res, next) => {
  const results = [];
  let queryString;
  let status;
  var tasksPerPage = Number(req.header('X-Page-Size'));
  var currentPage = req.header('X-Current-Page');
  let offSet = currentPage * tasksPerPage - 5;
  let args;
  if (req.query.status == 'all') {
    argsForCount = ['Select count(*) FROM taskslist'];
    argsForSelect = ['SELECT taskslist.* FROM taskslist ORDER BY id ASC offset ($1) limit ($2)', [offSet, tasksPerPage]];
  }
  else {
    if (req.query.status == 'complete') {
      status = true;
    }
    else {
      status = false;
    }
    argsForCount = ['Select count(*) FROM taskslist WHERE complete=($1)', [status]];
    argsForSelect = ['SELECT taskslist.*FROM taskslist WHERE complete=($1) ORDER BY id ASC offset ($2) limit ($3)', [status, offSet, tasksPerPage]];
  }
  argsForCountCompleted = ['Select count(*) FROM taskslist WHERE complete=($1)', [true]];
  argsForCountUncompleted = ['Select count(*) FROM taskslist WHERE complete=($1)', [false]];




  client.query(...argsForCountCompleted, (err, dbRes) => {
    if (err) {
      return res.status(400).json(err);
    }
    res.setHeader('X-Completed-Count', dbRes.rows[0].count);

    client.query(...argsForCountUncompleted, (err, dbRes) => {
      if (err) {
        return res.status(400).json(err);
      }
      res.setHeader('X-Uncompleted-Count', dbRes.rows[0].count);

      client.query(...argsForCount, (err, dbRes) => {
        if (err) {
          return res.json(err);
        }
        else {
          res.setHeader('X-Total-Count', dbRes.rows[0].count);

          client.query(...argsForSelect, (err, dbRes) => {
            if (err) {
              return res.status(400).json(err);
            }
            let result = dbRes.rows;
            return res.json(result);
          });
        }
      });
    });
  });
});

// Форматирование заданий
app.put('/tasks/:id', (req, res, next) => {
  console.log('Receive PUT1', req.body);
  const results = [];
  const id = req.params.id;
  // client = pgClient;
  let queryString = 'UPDATE taskslist SET ';
  queryString += Object
    .keys(req.body)
    .map(key => {
      return `${key}='${req.body[key]}'`;
    })
    .join(' ');
  queryString += ` where id=${id};`
  const query = client.query(queryString);
  query.on('end', function () {
    return res.json(results);
  });
});


// Удаление задания
app.delete('/tasks/:id', (req, res, next) => {
  const results = [];
  const id = req.params.id;
  const query = client.query('DELETE FROM taskslist WHERE id=($1)', [id]);
  query.on('end', () => {
    return res.json(results);
  });
});
