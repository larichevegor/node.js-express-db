let currentPage = 1;
//Установка чека на 'Все задания'
$("#radioButtonAll").prop("checked", true);
$(document).ready(function () {
    filterMassForPage(currentPage);
});
// Нажатие на "добавить задание"
$("#idOfPlusTask").on('click', function (event) {
    addTask();
});
// Переключение статуса
$(".fullTaskList").on('change', '.radio', function (event) {
    filterMassForPage(currentPage);
});


// Добавление заданий
function addTask() {
    var taskValue = $("#idOfInputArea").val();
    if (taskValue.trim() == '') {
        alert("ВВЕДИТЕ ЗАДАНИЕ!");
    }
    else {
        $.ajax({
            url: 'http://localhost:3000/tasks',
            data: { text: taskValue },
            type: 'POST',
            success: function (res) {
                console.log('Удачно добавлено в базу!', res);
                filterMassForPage(currentPage);
            },
            error: function () {
                alert('Error!')
            },
        });
        $("#idOfInputArea").val("");
    }
    return false;
}
// Отображение заданий
function show(tasksList) {
    $("#idOfToDoList").empty();
    for (let i = 0; i < tasksList.length; i++) {
        var elem = $('<div/>', {
            class: 'task',
            id: tasksList[i].id,
            append: $('')
                .add("<input class='box' type='checkbox'>")
                .add('<span class="class_task_text" id="taskText">' + tasksList[i].text + '</span>')
                .add('<input class="delete" type="button" class="delete" value="X"/>'),


        });
        $(elem).appendTo('#idOfToDoList');
        $('#' + tasksList[i].id + ' .box').prop('checked', tasksList[i].complete);

    }
}

// Удаление задания
$(".toDoList").on('click', '.delete', function (event) {
    var idOfParent = $(this).parents('.task').attr("id");
    $.ajax({
        url: ("http://localhost:3000/tasks/" + idOfParent),
        type: 'Delete',
        success: function (res) {
            console.log('Удалено');
            filterMassForPage(currentPage);
        },
        error: function () {
            alert('Error!')
        },
    });

});
// Изменение текста задания
$("#idOfToDoList").on('dblclick', '.class_task_text', function (event) {
    var idOfParent = $(this).parents('.task').attr("id");
    changeTask(idOfParent);
});
$("#idOfToDoList").on('dblclick', '.editTask', function (event) {
    var idOfParent = $(this).parents('.task').attr("id");
    var value = $('.editTask').val();
    changeInput(idOfParent, value);
});

// Изменение задания
function changeTask(elemId) {
    console.log(elemId);
    text = $('#' + elemId + ' #taskText').text();
    var input = $('<input />', {
        type: 'text',
        name: 'name',
        class: 'editTask',
        value: $('#' + elemId + ' #taskText').text(),
        on: {
            keydown: function (e) {
                if (e.which == 13) {
                    $('#' + elemId + ' #taskText').text(this.value);
                    if (this.value.trim() == '') {
                        alert("ВВЕДИТЕ ЗАДАНИЕ!");
                        $('#' + elemId + ' #taskText').text(text);
                    }
                    else
                    {
                    $.ajax({
                        url: ("http://localhost:3000/tasks/" + elemId),
                        data: { text: this.value },
                        type: 'PUT',
                        success: function (res) {
                            console.log('Удачно изменен текст!');
                            filterMassForPage(currentPage);
                        },
                        error: function () {
                            alert('Error!')
                        }
                    });
                    }
                }
            },
        }
    });
    $('#' + elemId + ' #taskText').html(input);
}
// Исправление двойного клика на поле
function changeInput(elemId, val) {
    $('#' + elemId + ' .editTask').text(val);
}

// Изменение статуса задания
$(".toDoList").on('change', '.box', function (event) {
    var idOfParent = $(this).parents('.task').attr("id");
    console.log(idOfParent);
    $.ajax({
        url: ("http://localhost:3000/tasks/" + idOfParent),
        data: JSON.stringify({ complete: $(this).prop("checked") }),
        type: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        success: function (res) {
            console.log('Удачно изменен статус!');
            filterMassForPage(currentPage);
        },
        error: function () {
            alert('Error!')
        },
    });
});


// Подсчет кол-ва заданий
function calc(completed, unCompleted) {
    $("#spanCompleted").text('Выполнено: ' + completed);
    $("#spanUncompleted").text('Не выполнено: ' + unCompleted);
}

const PAGE_SIZE = 5;
let totalCount = 0;

// Пагинация
function pagination(pageCount) {
    $("#idOfPages").empty();
    console.log(pageCount);
    var pageNumber = Math.ceil(pageCount / PAGE_SIZE);
    // let restOfNumber = pageCount%PAGE_SIZE;
    // console.log(restOfNumber);
    for (let i = 1; i <= pageNumber; i++) {
        var element = $('<li />', {
            class: 'page_number',
            id: i,
            on: {
                click: function getIdOfPage(event) {
                    page = this.id;
                    currentPage = page;
                    filterMassForPage(currentPage);
                }
            },
            append: $('')
                .add("<span>" + i + "</span>"),
        });
        $(element).appendTo('#idOfPages');
    }
}


// Фильтрация по странице
function filterMassForPage(currentPage) {
    let value = $('input[name=filter]:checked').val();

    let jReq = $.ajax({
        url: '/tasks?status=' + value,
        type: 'GET',
        headers: {
            'x-current-page': currentPage,
            'x-page-size': PAGE_SIZE
        },
        success: function (data) {
            totalCount = jReq.getResponseHeader('X-Total-Count');
            completedCount = jReq.getResponseHeader('X-Completed-Count');
            uncompletedCount = jReq.getResponseHeader('X-Uncompleted-Count');
            pagination(totalCount);
            show(data);
            calc(completedCount, uncompletedCount);
        },
        error: function () {
            alert('Error!')
        },
    });

}
